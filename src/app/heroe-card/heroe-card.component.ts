import {Component, Input, OnInit} from '@angular/core';
import {HeroesModel} from '../models/heroes';
import {weaponsTypes} from '../enums/weapons-types-enum';
import {colors} from '@angular/cli/utilities/color';

@Component({
  selector: 'heroe-card',
  templateUrl: './heroe-card.component.html',
  styleUrls: ['./heroe-card.component.css']
})
export class HeroeCardComponent implements OnInit {

  @Input() hero: HeroesModel;
  stats = [];
  armors = [];
  type = weaponsTypes;
  iconName = {Intelligence: {name: 'ra ra-book', color: 'blue'},
    Luck: {name: 'ra ra-perspective-dice-random', color: 'black'},
    Agility: {name: 'ra ra-player-dodge', color: '#8FBC8F'},
    Vigor: {name: 'ra ra-hearts', color: 'green'},
    Strength: {name: 'ra ra-muscle-up', color: 'red'}};

  constructor() { }

  ngOnInit() {
    Object.keys(this.hero.stats).forEach((key) => {
        this.stats.push({name: key , value: this.hero.stats[key]});
      });
    this.armors.push({piece: this.hero.helmet, icon: 'ra ra-helmet'},
      {piece: this.hero.chestplate, icon: 'ra ra-vest'},
      {piece: this.hero.gauntlet, icon: 'ra ra-hand'},
      {piece: this.hero.boots, icon: 'ra ra-boot-stomp'});
  }

  getIconColor(y: number) {
    const x = y * 255;
    // tslint:disable-next-line:no-shadowed-variable
    let colors;
    if (x > 125) {
      const redColor: number = 505 -  2 * x;
      colors = {
        r: redColor,
        g: 255
      };
    } else {
      const greenColor: number = x;
      colors = {
        r: 255,
        g: greenColor
      };
    }
    return colors;
  }

}
