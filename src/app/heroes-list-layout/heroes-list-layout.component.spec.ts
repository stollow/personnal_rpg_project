import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesListLayoutComponent } from './heroes-list-layout.component';

describe('HeroesListLayoutComponent', () => {
  let component: HeroesListLayoutComponent;
  let fixture: ComponentFixture<HeroesListLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesListLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesListLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
