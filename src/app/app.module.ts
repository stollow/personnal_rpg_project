import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeroesListLayoutComponent } from './heroes-list-layout/heroes-list-layout.component';
import {HeroesService} from './services/heroes.service';
import {HttpClientModule} from '@angular/common/http';
import { HeroeCardComponent } from './heroe-card/heroe-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroesListLayoutComponent,
    HeroeCardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    HeroesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
