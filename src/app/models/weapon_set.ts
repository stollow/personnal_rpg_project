import {Weapon} from './weapon';
import {Shield} from './shield';

export class WeaponSet {
  hands: number;
  weapons: Weapon[];
  shield: Shield;
  ammos: number;
  primary: boolean;

  constructor() {
  }

}
