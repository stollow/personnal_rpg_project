export class Weapon {
  damage: number;
  range: number;
  name: string;
  type: string;
  state: number;
  twoHanded: boolean;

  constructor() {
  }

}
