import {StatsModel} from './stats';
import {WeaponSet} from './weapon_set';

export class HeroesModel {
  name: string;
  sexe: string;
  race: string;
  gold: number;
  stats: StatsModel;
  chestplate: ArmorInterface;
  boots: ArmorInterface;
  gauntlet: ArmorInterface;
  helmet: ArmorInterface;
  config: WeaponSet[];


  constructor() {
  }

}
