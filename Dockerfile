FROM node:13.8.0-stretch
WORKDIR /home/node/app
COPY . .
RUN npm install --loglevel=warn
RUN npm install -g @angular/cli
USER node
CMD ng serve --host 0.0.0.0 --port 4200 --disable-host-check

